package ru.itgirls.sample_docker_app.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {
    @GetMapping("/")
    public String hello() {
        return "Hello Docker!";
    }
}
